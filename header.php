<div class="container-fluid header">
	<div class="row-fluid">
		<div class="span12">
			<div class="container inner_container" > 
				<div class="logo"></div>
				<div class="row">
					 <div class="col-xs-3"><div class="top_text">The future of production is now</div></div>
					 <div class="col-xs-6">
					 <input type="text" class="angle angle34 search">
					 	<div class="row main-menu">
					 		<div class="col-xs-2 menu-item"><a class="label" href="#">About Us</a></div>
					 		<div class="col-xs-2 menu-item"><a class="label" href="#">Events</a></div>
					 		<div class="col-xs-2 menu-item"><a class="label" href="#">How To</a></div>
					 		<div class="col-xs-2 menu-item"><a class="label" href="#">Q&A</a></div>
					 		<div class="col-xs-2 menu-item"><a class="label" href="#">Contact</a></div>
					 		<div class="col-xs-2 menu-item"><a class="label" href="#">e-Commerce</a></div>
					
					 	</div>
					 </div>
					 <div class="col-xs-3">
					 	<div class="row top-menu">
					 		 <a class="col-xs-4 menu-item"  href="#"><label class="label1">Log in</label><label class="label2">Guest</label></a>
					 		 <a class="col-xs-4 menu-item icon2" href="#"><label class="label1">Cart</label><label class="label2">8 Items</label></a>
					 		 <a class="col-xs-4 menu-item icon3" href="#"><label class="label1">Language</label><label class="label2">English</label></a>
					 	</div>
					 	<div class="row dropdowns">
					 		 <div class="col-xs-6"><a class="select1" href="#">Producer</a></div>
					 		 <div class="col-xs-6"><a class="select2" href="#">Client</a></div>
					 	</div>
					 </div>
				</div>
			</div>
		</div>
	</div>
</div>